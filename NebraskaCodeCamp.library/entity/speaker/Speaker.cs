﻿namespace NebraskaCodeCamp.library.entity.speaker
{
    public class Speaker
    {
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Img { get; set; }
        public string Web { get; set; }
        public string Location { get; set; }

        public string fullImageUri 
        {
            get { return "http://www.nebraskacodecamp.com/Content/Speakers/" + Img; }
        }
    }
}
