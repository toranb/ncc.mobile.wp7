﻿using System;
using NebraskaCodeCamp.library.entity.speaker;

namespace NebraskaCodeCamp.library.entity.session
{
    public class SessionClazz
    {
        public string Session { get; set; }
        public string Time { get; set; }
        public string Desc { get; set; }
        public string Room { get; set; }
        public Speaker Speaker { get; set; }

        public string speakerName
        {
            get { return Speaker.Name; }
        }

        public string formattedSessionName
        {
            get
            {
                if (Session.Length < 38)
                    return Session;

                var actualName = Session.Substring(0, 38);
                return actualName.Trim() + "...";
            }
        }

        public DateTime sortFriendlyStartTime
        {
            get { 
                var postHour = Time.IndexOf(":");
                var startTime = Time.Substring(0, postHour + 6);
                var actualDate = "01/01/2011 " + startTime;
                return Convert.ToDateTime(actualDate);
            }
        }
    }
}
