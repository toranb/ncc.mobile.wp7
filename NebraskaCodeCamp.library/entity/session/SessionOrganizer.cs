﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NebraskaCodeCamp.library.entity.session
{
    public class SessionOrganizer
    {
        public IEnumerable SortAndGroupSessionsByTime(IEnumerable<SessionClazz> sessions)
        {
            var sessionByTime = from s in sessions
                                orderby s.sortFriendlyStartTime.TimeOfDay
                                group s by s.Time
                                    into c
                                    select new Group<SessionClazz>(c.Key, c);

            return sessionByTime;
        }
    }
}
