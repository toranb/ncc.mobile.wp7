﻿using System.Collections.Generic;

namespace NebraskaCodeCamp.library.entity.session
{
    public class SessionResults
    {
        public IEnumerable<SessionClazz> data { get; set; }
    }
}
