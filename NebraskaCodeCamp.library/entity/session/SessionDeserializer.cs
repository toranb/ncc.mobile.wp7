﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace NebraskaCodeCamp.library.entity.session
{
    public class SessionDeserializer
    {
        public IEnumerable<SessionClazz> ParseSessionJsonAndReturnListOfSessions(string json)
        {
            IEnumerable<SessionClazz> sessions;
            try
            {
                sessions = JsonConvert.DeserializeObject<SessionResults>(json).data;
            }
            catch (Exception exception)
            {
                sessions = new List<SessionClazz>();
            }

            return sessions;
        }
    }
}
