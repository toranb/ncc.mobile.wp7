﻿using System.Collections.Generic;
using NebraskaCodeCamp.library.entity.session;

namespace NebraskaCodeCamp.library.presentation
{
    public interface ISessionListController
    {
        void ControllerCallBackWithSessions(IEnumerable<SessionClazz> sessions);
    }
}
