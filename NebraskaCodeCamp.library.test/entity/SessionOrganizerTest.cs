﻿using System.Collections.Generic;
using NebraskaCodeCamp.library.entity.session;
using NUnit.Framework;

namespace NebraskaCodeCamp.library.test.entity
{
    [TestFixture]
    public class SessionOrganizerTest
    {
        [Test]
        public void ListOfSessionsOrderedByTime()
        {
            var sut = new SessionOrganizer();
            var sessions = buildInputSessions();

            var sortedSessions = sut.SortAndGroupSessionsByTime(sessions);

            var sessionTimes = new List<string>();
            var groups = sortedSessions.GetEnumerator();
            while (groups.MoveNext())
            {
                var group = groups.Current as Group<SessionClazz>;
                sessionTimes.Add(group.FormattedTitle);
            }

            Assert.AreEqual(sessionTimes.Count, 5);
            Assert.AreEqual(sessionTimes[0], " 9:00 AM - 10:30 AM");
            Assert.AreEqual(sessionTimes[1], " 10:30 AM - 11:00 AM");
            Assert.AreEqual(sessionTimes[2], " 11:00 AM - 12:30 PM");
            Assert.AreEqual(sessionTimes[3], " 12:30 PM - 1:30 PM");
            Assert.AreEqual(sessionTimes[4], " 1:30 PM - 3:00 PM");
        }

        private IEnumerable<SessionClazz> buildInputSessions()
        {
            var first = new SessionClazz() { Time = "9:00 AM - 10:30 AM" };
            var second = new SessionClazz() { Time = "10:30 AM - 11:00 AM" };
            var third = new SessionClazz() { Time = "11:00 AM - 12:30 PM" };
            var fourth = new SessionClazz() { Time = "12:30 PM - 1:30 PM" };
            var last = new SessionClazz() { Time = "1:30 PM - 3:00 PM" };
            
            return new List<SessionClazz> { first,second,third,fourth,last };
        }
    }
}
