﻿using System.Windows;
using NebraskaCodeCamp.library.presentation;
using Microsoft.Phone.Controls;

namespace NebraskaCodeCamp.controllers
{
    public partial class SpeakerDetails : PhoneApplicationPage
    {
        private readonly ImageLookupController imageLookupController;

        public SpeakerDetails() : this (new ImageLookupController()) {}

        public SpeakerDetails(ImageLookupController imageLookupController)
        {
            InitializeComponent();

            this.imageLookupController = imageLookupController;

            SetSessionDetails();
        }

        public void SetSessionDetails()
        {
            var app = (App)Application.Current;
            var session = app.SelectedSessionClazz;

            loadSpeakerName.Text = session.Speaker.Name;
            loadSpeakerDesc.Text = session.Speaker.Bio;
            loadWebsite.Text = session.Speaker.Web;
            loadLocation.Text = session.Speaker.Location;

            this.Dispatcher.BeginInvoke(() => imageLookupController.SetImageFromUri(session.Speaker.fullImageUri, loadImg));
        }
    }
}