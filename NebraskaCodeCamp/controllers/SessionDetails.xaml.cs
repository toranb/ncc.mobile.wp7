﻿using System;
using System.Windows;
using NebraskaCodeCamp.library.presentation;
using Microsoft.Phone.Controls;

namespace NebraskaCodeCamp.controllers
{
    public partial class SessionDetails : PhoneApplicationPage
    {
        private readonly ImageLookupController imageLookupController;

        public SessionDetails() : this (new ImageLookupController()) {}

        public SessionDetails(ImageLookupController imageLookupController)
        {
            InitializeComponent();

            this.imageLookupController = imageLookupController;

            SetSessionDetails();
        }

        public void SetSessionDetails()
        {
            var app = (App)Application.Current;
            var session = app.SelectedSessionClazz;

            loadSessionName.Text = session.Session;
            loadSessionTime.Text = session.Time;
            loadSessionDesc.Text = session.Desc;
            loadSessionRoom.Text = session.Room;
            
            loadSpeakerName.Text = session.speakerName;
            this.Dispatcher.BeginInvoke(() => imageLookupController.SetImageFromUri(session.Speaker.fullImageUri, loadImg));
        }

        private void ViewSpeakerInfo_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/controllers/SpeakerDetails.xaml", UriKind.Relative));
        }
    }
}